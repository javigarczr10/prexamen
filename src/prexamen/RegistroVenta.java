/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prexamen;

/**
 *
 * @author safoe
 */
public class RegistroVenta {
   private int codigoVenta;
   private int cantidad;
   private float precio;
   private int tipo;
   
   
public RegistroVenta(){
    this.codigoVenta=0;
    this.cantidad=0;
    this.precio=0.0f;
    this.tipo=0;
}

    public RegistroVenta(int codigoVenta, int cantidad, float precio, int tipo) {
        this.codigoVenta = codigoVenta;
        this.cantidad = cantidad;
        this.precio = precio;
        this.tipo = tipo;
    }

public RegistroVenta(RegistroVenta otro){
    this.codigoVenta=otro.codigoVenta;
    this.cantidad=otro.cantidad;
    this.precio=otro.precio;
    this.tipo=otro.tipo;
    
}

    public int getCodigoVenta() {
        return codigoVenta;
    }

    public void setCodigoVenta(int codigoVenta) {
        this.codigoVenta = codigoVenta;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

public float CalcularCostoVenta(){
    float costoventa=0.0f;
    costoventa=this.precio*this.cantidad;
    return costoventa;
}

public float CalcularImpuesto(){
    float impuesto=0.0f;
    impuesto=.16f*this.CalcularCostoVenta();
    return impuesto;
}
public float CalcularTotalPagar(){
    float totalpagar=0.0f;
    totalpagar=this.CalcularCostoVenta()+this.CalcularImpuesto();
    return totalpagar;
}

    void setTipo(double d) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
